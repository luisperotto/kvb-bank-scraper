const fastify = require("fastify")({ logger: false });
const MainConfig = require("../src/config/main");
const {
  getTransactions,
  getBalance,
} = require("../src/controllers/transaction");
const { printMsg } = require("../src/controllers/helper");

fastify.get("/getTransactions", async (req, res) => {
  await getBalance();
  return {
    success: true,
    transactions: (await getTransactions()) || [],
  };
});

async function start() {
  let config = await MainConfig();
  let port = config.account.api_server_port;

  try {
    await fastify.listen({ port });
    printMsg(`Bot Server Started: 127.0.0.1:${port}`);
  } catch (err) {
    console.error(err);
    fastify.log.error(err);
    process.exit(1);
  }
}

(async function () {
  await start();
})();
