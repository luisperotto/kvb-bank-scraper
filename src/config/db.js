const { ACCOUNT_NUMBERS } = require(".");
const getBankAccounts = require("../db/bank_account");
const getBankScrapperConfig = require("../db/bank_scraper_config");

async function DB(bank) {
  let data = {
    bank_name: bank.name,
    api_data_dir: bank.api_data_dir,
    base_data_dir: bank.base_data_dir,

    start_scraper: false,
    url: "",
    get_statement_delay_time: 0,
  };

  let bank_account = await getBankAccounts(data.bank_name, ACCOUNT_NUMBERS);
  let bank_scraper_config = await getBankScrapperConfig(data.bank_name);

  data.account = bank_account.data;
  data.url = bank_scraper_config.data.url;
  data.get_statement_delay_time =
    bank_scraper_config.data.get_statement_delay_time;

  return data;
}

module.exports = DB;
