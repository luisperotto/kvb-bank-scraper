const { ACCOUNT_NUMBERS } = require(".");
const DB = require("./db");

async function MainConfig() {
  let bank = {
    name: "KVB Bank",
    api_data_dir: "../../bank-scraper-api/banks/kvb/" + ACCOUNT_NUMBERS,
    base_data_dir: "data",
  };

  let config = await DB(bank);
  return config;
}

module.exports = MainConfig;
