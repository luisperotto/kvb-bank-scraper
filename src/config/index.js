require("dotenv").config({ path: ".env" });

let config = process.env;

let APP_NAME = config.APP_NAME;
let HEADLESS_MODE = config.HEADLESS_MODE === "true" ? true : false;
let AUTH_TOKEN = config.AUTH_TOKEN;
let BANK_SCRAPER_API = config.BANK_SCRAPER_API;
let ACCOUNT_NUMBERS = process.argv[2];
let ACCOUNT_STATEMENT = process.env.ACCOUNT_STATEMENT;

const BANK_SCRAPER_API_CONFIG = {
  headers: {
    Authorization: `Bearer ${AUTH_TOKEN}`,
  },
};

module.exports = {
  APP_NAME,
  HEADLESS_MODE,
  AUTH_TOKEN,
  BANK_SCRAPER_API,
  ACCOUNT_NUMBERS,
  BANK_SCRAPER_API_CONFIG,
  ACCOUNT_STATEMENT,
};
