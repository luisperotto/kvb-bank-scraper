const moment = require("moment");
const color = require("colors");
const MainConfig = require("../config/main");
const { default: axios } = require("axios");
const jsonfile = require("jsonfile");
const { BANK_SCRAPER_API, BANK_SCRAPER_API_CONFIG } = require("../config");
const updateById = require("../db/updateById");

function printMsg(msg) {
  let txt = `${moment().format()} - [KVB Bank]`;
  console.log(color.yellow(txt), color.green(msg));
}

async function getConfig() {
  let config = await MainConfig();
  return config;
}

async function checkFailedLogin() {
  let config = await getConfig();
  if (config.account.failed_login_count < 1) {
    await updateById(config.account.id, {
      failed_login_count: config.account.failed_login_count + 1,
    });
    await sendTelegram(
      "Failed login, Count " +
        (parseInt(config.account.failed_login_count) + 1),
      true
    );
    printMsg("failed 1");
    printMsg("restart instance");
    await restartInstance();
  } else {
    await updateById(config.account.id, {
      failed_login_count: 0,
      scraper_status: "stopped",
      updated_at: new Date(),
    });
    printMsg("failed 2");
    await sendTelegram(
      "Login Failed for 2x, Instance Stopped, Please try to restart instance in a few minutes",
      true
    );
    printMsg("stop instance");
    await stopInstance();
  }
}

async function sendTelegram(message, with_photo = false) {
  try {
    const config = await getConfig();

    const response = await axios({
      baseURL: BANK_SCRAPER_API,
      url: "telegram/send",
      ...BANK_SCRAPER_API_CONFIG,
      params: {
        account_number: config.account?.account_numbers,
        msg: message,
        with_photo,
      },
    })
      .then((response) => {
        return {
          success: true,
          data: response?.data?.data,
        };
      })
      .catch((err) => {
        return {
          success: false,
          data: err?.response?.data,
        };
      });

    return response;
  } catch (error) {
    throw error;
  }
}

async function sendImage(message) {
  const config = await getConfig();
  let txt = `[ ${moment().format(
    "YYYY-MM-DD HH-mm-ss"
  )} ][newline] ${message} [newline]${config.bank_name} - ${
    config.account.account_numbers
  } - ${config.account.account_holder_name}`;

  let response = await sendTelegram(txt, true);
  return response;
}

async function getCode() {
  const config = await getConfig();

  let scrapperConfig = jsonfile.readFileSync(
    `${config.api_data_dir}/config.json`
  );

  return scrapperConfig.code;
}

async function fetchCode() {
  const config = await getConfig();
  await jsonfile.writeFile(`${config.api_data_dir}/config.json`, {
    code: null,
  });

  await sendImage("SOLVE CAPTCHA");
  let captchaCode = await getCode();
  while (!captchaCode) {
    printMsg("waiting for captcha....");
    await new Promise((r) => setTimeout(r, 2000));
    captchaCode = await getCode();
  }
  return captchaCode;
}

async function restartInstance() {
  printMsg("Restarting...");
  const config = await getConfig();
  let response = await axios({
    baseURL: BANK_SCRAPER_API,
    url: "restart",
    ...BANK_SCRAPER_API_CONFIG,
    params: {
      name: config?.account?.account_numbers,
    },
  })
    .then((res) => {
      return {
        status: true,
        data: res.data.data,
      };
    })
    .catch((err) => {
      return {
        status: false,
        data: err.response.data,
      };
    });
  console.log(response);
  return response;
}
async function stopInstance() {
  const config = await getConfig();

  try {
    await fetch(
      `${BANK_SCRAPER_API}/stop?name=${config.account.account_numbers}`,
      BANK_SCRAPER_API_CONFIG
    );
  } catch (error) {
    console.error(error);
  }
}

module.exports = {
  printMsg,
  sendTelegram,
  restartInstance,
  sendImage,
  fetchCode,
  checkFailedLogin,
};
