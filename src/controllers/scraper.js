const { ACCOUNT_STATEMENT } = require("../config");
const MainConfig = require("../config/main");
const {
  printMsg,
  sendTelegram,
  sendImage,
  fetchCode,
  checkFailedLogin,
  restartInstance,
} = require("./helper");
const moment = require("moment");
const updateById = require("../db/updateById");

class Scraper {
  constructor(context) {
    this.context = context;
    this.page = null;
    this.config = null;
    this.newPage = null;
  }
  async getConfig() {
    this.config = await MainConfig();
  }
  async start() {
    try {
      await this.getConfig();
      console.log(this.config);
      printMsg("Go to Page ....");

      this.page = await this.context.newPage();
      await this.page.goto(this.config?.url);
      await this.login();
      await this.getStatement(this.newPage);
      printMsg("getStatement Success");
      await sendTelegram("Success Bot Running");
      printMsg("waiting for schedule");
      await this.getStatementSchedule();
    } catch (error) {
      console.error(error);
      await sendTelegram("Failed Timed out 300000ms, access page");
      await sendTelegram("Restarting...");
      await restartInstance();
    }
  }
  async login() {
    try {
      printMsg("Login Page");
      await new Promise((r) => setTimeout(r, 5 * 1000));
      printMsg("Fill loginID");
      await this.page
        .locator(
          `#TDlogin > table > tbody > tr:nth-child(1) > td:nth-child(1) > input`
        )
        .type(this.config.account.username);
      printMsg("Fill password");
      await this.page
        .locator(
          `#TDlogin > table > tbody > tr:nth-child(1) > td:nth-child(2) > input[type=password]`
        )
        .type(this.config.account.password);

      await new Promise((r) => setTimeout(r, 5 * 1000));

      await this.page.screenshot({
        path: this.config.api_data_dir + "/photo.png",
      });

      let code = await fetchCode();
      await sendTelegram("Processing, Submitted Captcha: " + code);

      await this.page.locator("#fldcaptcha").type(code);
      await this.page
        .locator(
          "#TDlogin > table > tbody > tr:nth-child(1) > td:nth-child(2) > select"
        )
        .selectOption("L");
      // await new Promise((r) => setTimeout(r, 20 * 1000));
      let [newPage] = await Promise.all([
        this.page.waitForEvent("popup"),
        this.page
          .locator(
            `div[style="padding-left:22px;padding-right:22px;margin-left:50px;"]`
          )
          .click(),
      ]);

      await newPage.waitForLoadState();
      await new Promise((r) => setTimeout(r, 30 * 1000));
      console.log(await newPage.title());

      let check = await this.checkChapta(newPage);
      if (!check) {
        await newPage.screenshot({
          path: this.config.api_data_dir + "/photo.png",
        });
        await sendImage("INVALID CAPTCHA");
        await checkFailedLogin();
        printMsg("restart");
      }

      this.newPage = newPage;
    } catch (error) {
      console.error(error);
      await this.page.screenshot({
        path: this.config.api_data_dir + "/photo.png",
      });
      await checkFailedLogin();
      printMsg("Restart Instance");
    }
  }

  async checkChapta(page) {
    try {
      let check = await page.locator(
        "body > table > tbody > tr:nth-child(2) > td > div > table > tbody > tr:nth-child(1) > td"
      );

      if (await check.isVisible()) {
        return false;
      } else {
        return true;
      }
    } catch (error) {
      return true;
    }
  }

  async getStatement(page) {
    try {
      printMsg("Open Activity");
      let pageFrameLeft = await page.frameLocator(`frame[name="frame_menu"]`);
      let pageFrame = await page.frameLocator(`frame[name="frame_txn"]`);
      await pageFrameLeft.locator("#RRAAClink").click();
      printMsg("click activity");
      await new Promise((r) => setTimeout(r, 10 * 1000));
      printMsg("waiting account");
      await pageFrame
        .locator("select[name='fldacctno']")
        .waitFor({ state: "visible" });
      printMsg(`account of ${ACCOUNT_STATEMENT}`);
      await pageFrame
        .locator("select[name='fldacctno']")
        .selectOption(ACCOUNT_STATEMENT);
      await pageFrame.locator("select[name='fldsearch']").waitFor({
        state: "visible",
      });

      await pageFrame.locator(`input[name="fldsubmit"]`).waitFor({
        state: "visible",
      });

      await pageFrame.locator(`input[name="fldsubmit"]`).click();

      printMsg("wait for statement");

      await new Promise((r) => setTimeout(r, 10 * 1000));

      await pageFrame
        .locator(`select[name="fldsearchformat"]`)
        .waitFor({ state: "visible" });
      await pageFrame
        .locator(`select[name="fldsearchformat"]`)
        .selectOption("05");

      await new Promise((r) => setTimeout(r, 10 * 1000));
      printMsg("downloading");
      await pageFrame.locator("#flddownload").waitFor({ state: "visible" });
      printMsg(".......................");
      let [download] = await Promise.all([
        page.waitForEvent("download"),
        pageFrame.locator("#flddownload").click(),
      ]);

      download.saveAs(
        `${this.config.base_data_dir}/transaction_kvb_${moment().format(
          "YYYY-MM-DD"
        )}.csv`
      );

      printMsg("download Success");
    } catch (error) {
      printMsg("Restart Instance");
      console.error(error);
      await restartInstance();
    }
  }
  async getStatementSchedule() {
    let time = 0;
    updateById(this.config.account.id, {
      scraper_status: "running",
    });
    if (this.config.get_statement_delay_time == -1) {
      time = Math.floor(Math.random() * 15) + 30;
    } else {
      time = this.config.get_statement_delay_time;
    }
    printMsg("waiting schedule for " + time + "second");

    await new Promise((r) => setTimeout(r, time * 1000));
    await this.getStatement(this.newPage);
    printMsg("waiting for schedule");
    await this.getStatementSchedule();
  }
}

module.exports = Scraper;
