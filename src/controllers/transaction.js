const moment = require("moment");
const csv = require("csvtojson");
const fs = require("fs").promises;
const MainConfig = require("../config/main");
const updateById = require("../db/updateById");

async function getTransactions() {
  let dir = await fs.readdir(`data`);
  if (dir && dir.length > 0) {
    let file = dir
      .filter((el) => {
        return el.indexOf(".") !== 0 && el.slice(-4) === ".csv";
      })
      .find((el) => {
        let f = el.split("_");
        let names = f[f.length - 1];
        names = names.replace(names.slice(-4), "");
        return moment().isSame(names, "dates");
      });
    if (file) {
      let transactions = await csv({
        noheader: true,
        delimiter: "\n",
        output: "line",
      }).fromFile(`data/${file}`);
      if (transactions.length > 0) {
        index = transactions.findIndex((el) => el.includes("Transaction Date"));
        let cut = transactions.slice(0, index);
        transactions = transactions.filter((el) => {
          return !cut.find((c) => c === el) && el !== "";
        });

        if (transactions.length > 0) {
          transactions = transactions.map((el, index) => {
            if (index !== 0) {
              let datas = el.split('"');
              let data = datas[0].split(",");
              let credit = parseFloat(datas[3].replace(/\,/g, ""));
              let debit = parseFloat(datas[1].replace(/\,/g, ""));
              // let credit = parseFloat(data[6].replace('"', "")) || 0;
              // let debit = parseFloat(data[5].replace('"', "")) || 0;
              let obj = {
                transaction_date: moment(data[0], "DD-MM-YYYY HH:mm:ss").format(
                  "YYYY-MM-DD"
                ),
                value_date: moment(data[0], "DD-MM-YYYY HH:mm:ss").format(
                  "YYYY-MM-DD"
                ),
                description: data[4].trim() || "",
                reference_number: data[3] || "",
                utr:
                  (data[4] || "")
                    .split("-")
                    .find((ts) => !isNaN(ts) && ts.length === 12) || "",
                method_type: (data[4] || "").includes("UPI") ? "UPI" : "Other",
                balance: parseFloat(datas[5].replace(/\,/g, "")) || 0,
                from: "",
                to: "",
                amount: credit > 0 ? credit : debit,
                is_balance_out: credit > 0 ? false : true,
              };
              let transaction_code =
                obj.description
                  .split("-")
                  .find((el) => el.includes("DP") && el.length === 15) || "";
              obj.transaction_code = transaction_code;
              return obj;
            } else {
              return null;
            }
          });

          transactions = transactions.filter((el) => el !== null);

          transactions.reverse();
          transactions.map((e, index) => {
            if (index !== 0) {
              transactions[index] = {
                ...transactions[index],
                begin_balance:
                  (transactions[index - 1] &&
                    transactions[index - 1].balance) ||
                  0,
              };
            } else {
              transactions[index].begin_balance =
                parseFloat(
                  parseFloat(
                    [index] && transactions[index].is_balance_out
                      ? parseFloat(transactions[index].balance) +
                          parseFloat(transactions[index].amount)
                      : parseFloat(transactions[index].balance) -
                          parseFloat(transactions[index].amount)
                  ).toFixed(2)
                ) || 0;
            }
          });
          transactions.reverse();
          return transactions;
        }
      }
    }
  }
}

async function getBalance() {
  let config = await MainConfig();

  let transactions = (await getTransactions()) || [];
  if (transactions.length > 0) {
    transactions = transactions[0];
    let balance = transactions.balance || 0;
    updateById(config.account.id, {
      balance,
    });
  }
}

module.exports = {
  getTransactions,
  getBalance,
};
