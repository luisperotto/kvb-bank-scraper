const { default: axios } = require("axios");
const {
  BANK_SCRAPER_API,
  BANK_SCRAPER_API_CONFIG,
  ACCOUNT_NUMBER,
} = require("../config");

async function getBankScrapperConfig(bank_name) {
  const response = axios({
    baseURL: BANK_SCRAPER_API,
    url: "db/bank-scraper-configs",
    ...BANK_SCRAPER_API_CONFIG,
    params: {
      bank_name,
      account_number: ACCOUNT_NUMBER,
    },
  })
    .then((response) => {
      return {
        status: true,
        data: response?.data?.data,
      };
    })
    .catch((error) => {
      return {
        status: false,
        data: error?.response?.data,
      };
    });
  return response;
}

module.exports = getBankScrapperConfig;
