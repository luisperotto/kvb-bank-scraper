const { default: axios } = require("axios");
const { BANK_SCRAPER_API, BANK_SCRAPER_API_CONFIG } = require("../config");

async function updateById(id, data) {
  let response = await axios({
    baseURL: BANK_SCRAPER_API,
    method: "post",
    url: "db/bank-account/" + id,
    ...BANK_SCRAPER_API_CONFIG,
    data: data,
  })
    .then((response) => {
      return { status: true, data: response?.data?.data };
    })
    .catch((error) => {
      return { status: false, data: error?.response?.data };
    });
  return response;
}

module.exports = updateById;
