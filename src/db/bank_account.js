const { default: axios } = require("axios");
const { BANK_SCRAPER_API, BANK_SCRAPER_API_CONFIG } = require("../config");

async function getBankAccounts(bank_name, bank_account_number) {
  const response = await axios({
    url: "db/bank-account",
    baseURL: BANK_SCRAPER_API,
    ...BANK_SCRAPER_API_CONFIG,
    params: {
      bank_name,
      bank_account_number,
    },
  })
    .then((response) => {
      return { status: true, data: response?.data?.data };
    })
    .catch((error) => {
      return { status: false, data: error?.response?.data };
    });

  return response;
}

module.exports = getBankAccounts;
