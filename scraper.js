const { devices } = require("@playwright/test");
const playwright = require("playwright");
const { HEADLESS_MODE } = require("./src/config");
const { printMsg } = require("./src/controllers/helper");
const Scraper = require("./src/controllers/scraper");

async function start() {
  let device = devices["Desktop Chrome"];
  let config = {
    headless: true,
  };
  if (!HEADLESS_MODE) {
    config.headless = false;
  }
  printMsg("Launching Scraper....");

  let browser = await playwright.chromium.launch(config);

  let context = await browser.newContext({
    ...device,
  });

  context.setDefaultTimeout(300 * 1000);
  context.setDefaultNavigationTimeout(300 * 1000);

  let scraper = new Scraper(context);
  await scraper.start();
}

(async () => {
  await start();
})();
